/*
 * Neo4JClientTest.java
 * 
 * Copyright (C) 2012 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j.client;

import java.util.List;
import junit.framework.TestCase;
import org.json.simple.JSONObject;
import org.reco4j.graph.neo4j.client.bean.Node;
import org.reco4j.graph.neo4j.client.bean.Plugin;
import org.reco4j.graph.neo4j.client.bean.Relationship;
import org.reco4j.graph.neo4j.client.connection.Neo4JConfiguration;

/**
 *
 * @author Alessandro Negro <alessandro.negro at reco4j.org>
 */
public class Neo4JClientTest extends TestCase
{

  private static final String SERVER_ROOT_URI = "http://localhost:7474/db/data/";
  private static final String RECO4J_RECOMMENDER_PLUGIN = "Reco4jRecommender";
  private static final String RECO4j_GET_ITEMS = "get_items";
  private static final String RECO4j_GET_USERS = "get_users";
  private static final String RECO4j_GET_ALL_RATINGS = "get_all_ratings";
  private Neo4JClient client;

  public Neo4JClientTest(String testName)
  {
    super(testName);
    Neo4JConfiguration conf = new Neo4JConfiguration();
    conf.set(Neo4JConfiguration.SERVER_ROOT_URI, SERVER_ROOT_URI);
    client = new Neo4JClient(conf);
  }

  @Override
  protected void setUp() throws Exception
  {
    super.setUp();

  }

  @Override
  protected void tearDown() throws Exception
  {
    super.tearDown();
  }

  /**
   * Test of getPlugins method, of class Neo4JClient.
   */
  public void testGetPlugins()
  {
    System.out.println("getPlugins");
    List result = client.getPlugins();
    assertNotNull(result);
    assertTrue(result.size() > 0);
  }

  /**
   * Test of getPlugin method, of class Neo4JClient.
   */
  public void testGetPlugin()
  {
    System.out.println("getPlugin");
    Plugin result = client.getPlugin(RECO4J_RECOMMENDER_PLUGIN);
    assertNotNull(result);
    assertEquals("No Reco4J plugin found!", result.getName(), RECO4J_RECOMMENDER_PLUGIN);
  }

  /**
   * Test of getNodes method, of class Neo4JClient.
   */
  public void testGetNodes()
  {
    System.out.println("getNodes");
    Plugin reco4jPlugin = client.getPlugin(RECO4J_RECOMMENDER_PLUGIN);
    assertNotNull(reco4jPlugin);

    String query = reco4jPlugin.getContent(RECO4j_GET_ITEMS);
    assertNotNull(query);

    List<Node> result = client.getNodes(query);
    assertTrue(result.size() > 0);

    query = reco4jPlugin.getContent(RECO4j_GET_USERS);
    result = client.getNodes(query);
    assertTrue(result.size() > 0);
  }

  /**
   * Test of getNode method, of class Neo4JClient.
   */
  public void testGetNode_int()
  {
    System.out.println("getNode");
    int id = 1;
    Node result = client.getNode(id);
    assertNotNull(result);
    assertTrue(result.getSelf().substring(result.getSelf().lastIndexOf("/") + 1).equalsIgnoreCase("" + id));
  }

  /**
   * Test of getRelationships method, of class Neo4JClient.
   */
  public void testGetRelationships()
  {
    System.out.println("getNodes");
    Plugin reco4jPlugin = client.getPlugin(RECO4J_RECOMMENDER_PLUGIN);
    assertNotNull(reco4jPlugin);

    String query = reco4jPlugin.getContent(RECO4j_GET_ALL_RATINGS);
    assertNotNull(query);

    List<Relationship> result = client.getRelationships(query);
    assertTrue(result.size() > 0);
  }

  /**
   * Test of getRelationship method, of class Neo4JClient.
   */
  public void testGetRelationship_int()
  {
    System.out.println("getRelationship");
    int id = 1;
    Relationship result = client.getRelationship(id);
    assertNotNull(result);
    assertTrue(result.getSelf().substring(result.getSelf().lastIndexOf("/") + 1).equalsIgnoreCase("" + id));

    id = 2;
    result = client.getRelationship(id);
    assertNotNull(result);
    assertTrue(result.getSelf().substring(result.getSelf().lastIndexOf("/") + 1).equalsIgnoreCase("" + id));
  }

  /**
   * Test of addNode method, of class Neo4JClient.
   */
  public void testAddNode()
  {
    System.out.println("addNode");
    String result = client.addNode();
    assertNotNull(result);
    Node newNode = client.getNode(result);
    assertNotNull(newNode);
  }

  /**
   * Test of addRelationship method, of class Neo4JClient.
   */
  public void testAddRelationship()
  {
    System.out.println("addRelationship");
    Node start = client.getNode(1);
    Node end = client.getNode(2);
    Relationship rel = new Relationship();
    rel.setStart(start.getSelf());
    rel.setEnd(end.getSelf());
    rel.setType("test");

    JSONObject data = new JSONObject();
    data.put("name", "foo");
    data.put("num", new Integer(100));
    rel.setData(data);
    String result = client.addRelationship(rel);
    assertNotNull(result);
    Relationship remoteRelationship = client.getRelationship(result);
    assertNotNull(remoteRelationship);
    assertTrue(remoteRelationship.getData().get("name").toString().equalsIgnoreCase("foo"));
  }

  /**
   * Test of addProperty method, of class Neo4JClient.
   */
  public void testAddPropertyToNode()
  {
    System.out.println("addPropertyToNode");
    String nodeURI = client.addNode();
    assertNotNull(nodeURI);
    Node node = client.getNode(nodeURI);
    String property = "testProperty";
    Object value = "1";
    int result = client.addProperty(node, property, value);
    assertEquals(result, 0);
  }

  /**
   * Test of addProperty method, of class Neo4JClient.
   */
  public void testAddPropertyToRelationship()
  {
    System.out.println("addPropertyToRelationship");
    Relationship rel = client.getRelationship(1);
    String property = "testProperty";
    Object value = "1";
    int result = client.addProperty(rel, property, value);
    assertEquals(result, 0);
  }
}
