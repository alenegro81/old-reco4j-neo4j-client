/*
 * Plugin.java
 * 
 * Copyright (C) 2012 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j.client.bean;

import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 * @author Alessandro Negro <alessandro.negro at reco4j.org>
 */
public class Plugin
{
  private String name;
  private List<String> operations;
  private LinkedHashMap content;

  public Plugin(String name, LinkedHashMap content)
  {
    this.name = name;
    this.content = content;
  }

  public String getName()
  {
    return name;
  }

  public List<String> getOperations()
  {
    return operations;
  }

  public LinkedHashMap getContentMap()
  {
    return content;
  }
  
  public String getContent(String name)
  {
    return (String) content.get(name);
  }
  public String toString()
  {
    return name + "=>" + content;
  }
}
