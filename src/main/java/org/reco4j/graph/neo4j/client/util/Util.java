/*
 * Util.java
 * 
 * Copyright (C) 2012 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j.client.util;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.reco4j.graph.neo4j.client.bean.Node;
import org.reco4j.graph.neo4j.client.bean.Relationship;

/**
 *
 * @author Alessandro Negro <alessandro.negro at reco4j.org>
 */
public class Util
{

  private static final Logger logger = Logger.getLogger(Util.class.getName());

  public static JSONArray getArray(JSONParser parser, Object response)
  {
    JSONArray nodesArray = null;
    try
    {
      Object obj = parser.parse(response.toString());
      nodesArray = (JSONArray) obj;
    }
    catch (ParseException ex)
    {
      logger.log(Level.SEVERE, "Error while parsing response", ex);
    }
    return nodesArray;
  }

  public static Node parseNode(JSONObject next)
  {
    Node node = new Node();
    node.setData((JSONObject) next.get("data"));
    node.setSelf((String) next.get("self"));
    return node;
  }

  public static Relationship parseRelationship(JSONObject next)
  {
    Relationship rel = new Relationship();
    rel.setData((JSONObject) next.get("data"));
    rel.setStart((String) next.get("start"));
    rel.setEnd((String) next.get("end"));
    rel.setType((String) next.get("type"));
    rel.setSelf((String) next.get("self"));
    return rel;
  }

  public static String toJsonNameValuePairCollection(String name,
          Object value)
  {
    if (value instanceof String)
      return String.format("{ \"%s\" : \"%s\" }", name, value);
    else
      return String.format("{ \"%s\" : %s }", name, value.toString());
  }

  public static String generateJsonRelationship(String endNode,
          String relationshipType, String... jsonAttributes)
  {
    StringBuilder sb = new StringBuilder();
    sb.append("{ \"to\" : \"");
    sb.append(endNode);
    sb.append("\", ");

    sb.append("\"type\" : \"");
    sb.append(relationshipType);
    if (jsonAttributes == null || jsonAttributes.length < 1)
    {
      sb.append("\"");
    }
    else
    {
      sb.append("\", \"data\" : ");
      for (int i = 0; i < jsonAttributes.length; i++)
      {
        sb.append(jsonAttributes[i]);
        if (i < jsonAttributes.length - 1)
        { // Miss off the final comma
          sb.append(", ");
        }
      }
    }

    sb.append(" }");
    return sb.toString();
  }
}
