/*
 * Neo4JClient.java
 * 
 * Copyright (C) 2012 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j.client;

import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.reco4j.graph.neo4j.client.bean.Node;
import org.reco4j.graph.neo4j.client.bean.Plugin;
import org.reco4j.graph.neo4j.client.bean.Relationship;
import org.reco4j.graph.neo4j.client.connection.Neo4JConfiguration;
import org.reco4j.graph.neo4j.client.connection.Neo4JConnection;
import org.reco4j.graph.neo4j.client.connection.Neo4JConnectionManager;
import static org.reco4j.graph.neo4j.client.util.Util.*;

/**
 * This class is the entry point of the library. It allows accessing main Neo4J functionalities.
 * Where possible it embed into beans the results of the queries like Node and Relations.
 * @author Alessandro Negro <alessandro.negro at reco4j.org>
 */
public class Neo4JClient
{

  private static final Logger logger = Logger.getLogger(Neo4JClient.class.getName());
  private JSONParser jparser;
  
  public Neo4JClient(Neo4JConfiguration conf)
  {
    Neo4JConnectionManager.getInstance().setDbConfiguration(conf);
  }

  public Plugin getPlugin(String pluginName)
  {
    Plugin result = null;
    for (Plugin plugin : getPlugins())
      if (plugin.getName().equalsIgnoreCase(pluginName))
        result = plugin;
    return result;
  }

  public List<Plugin> getPlugins()
  {
    JSONObject result = get();
    if (result == null)
      return null;

    ContainerFactory containerFactory = new ContainerFactory()
    {
      public List creatArrayContainer()
      {
        return new LinkedList();
      }

      public Map createObjectContainer()
      {
        return new LinkedHashMap();
      }
    };

    List<Plugin> plugins = new ArrayList<Plugin>();
    try
    {
      Map json = (Map) getParser().parse(result.get("extensions").toString(), containerFactory);
      Iterator iter = json.entrySet().iterator();
      while (iter.hasNext())
      {
        Map.Entry entry = (Map.Entry) iter.next();
        Plugin plugin = new Plugin((String) entry.getKey(), (LinkedHashMap) entry.getValue());
        plugins.add(plugin);
      }
    }
    catch (ParseException pe)
    {
      logger.log(Level.SEVERE, "Error while parsing extensions.", pe);
    }

    return plugins;
  }

  public List<Node> getNodes(String query)
  {
    List<Node> result = new ArrayList<Node>();
    String response = Neo4JConnectionManager.getInstance()
            .getConnection()
            .post(query);
    JSONArray nodesArray = getArray(getParser(), response);
    if (nodesArray != null)
    {
      Iterator iterator = nodesArray.iterator();
      while (iterator.hasNext())
      {
        Node node = parseNode((JSONObject)iterator.next());
        result.add(node);
      }
    }
    return result;
  }
  
  public Node getNode(int id)
  {
    return getNode("node/" + id);
  }
  
  public Node getNode(String id)
  {
    String response = Neo4JConnectionManager.getInstance()
            .getConnection()
            .get(id);
    try 
    {
      Node node = parseNode((JSONObject)getParser().parse(response.toString()));
      return node;
    }
    catch (ParseException pe)
    {
      logger.log(Level.SEVERE, "Error while parsing extensions.", pe);
      return null;
    }
  }

  public List<Relationship> getRelationships(String query)
  {
    String response = Neo4JConnectionManager.getInstance()
            .getConnection()
            .post(query);
    JSONArray relationshipsArray = getArray(getParser(), response);
    List<Relationship> result = new ArrayList<Relationship>();
    if (relationshipsArray != null)
    {
      Iterator iterator = relationshipsArray.iterator();
      while (iterator.hasNext())
      {
        final JSONObject name = (JSONObject)iterator.next();
        //System.out.println(name);
        Relationship rel = parseRelationship(name);
        result.add(rel);
      }
    }  
    return result;
  }
  
  public InputStream getInputStream(String query)
  {
    return Neo4JConnectionManager.getInstance()
            .getConnection()
            .postInputStream(query);
  }
  
  public Relationship getRelationship(int id)
  {
    return getRelationship("relationship/" + id);
  }
  
  public Relationship getRelationship(String id)
  {
    String response = Neo4JConnectionManager.getInstance()
            .getConnection()
            .get(id);
    try 
    {
      Relationship rel = parseRelationship((JSONObject)getParser().parse(response.toString()));
      return rel;
    }
    catch (ParseException pe)
    {
      logger.log(Level.SEVERE, "Error while parsing extensions.", pe);
      return null;
    }
  }

  public String addNode()
  {
    String nodeEntryPointUri = "node";
    URI nodeURI = Neo4JConnectionManager.getInstance()
            .getConnection()
            .postJSON(nodeEntryPointUri, "{}");
    if (nodeURI != null)
      return nodeURI.toString();
    else 
      return null;
  }

  public String addRelationship(Relationship rel)
  {
    String startNode = rel.getStart() + "/relationships";
    String endNode = rel.getEnd();
    String relationshipJson = generateJsonRelationship(endNode,
            rel.getType(), rel.getData().toJSONString());

    URI relationshipURI = Neo4JConnectionManager.getInstance()
            .getConnection()
            .postJSON(startNode, relationshipJson);
    if (relationshipURI != null)
      return relationshipURI.toString();
    else 
      return null;
  }

  public int addProperty(Node node, String property, Object value)
  {
    String nodeEntryPointUri = node.getSelf() + "/properties/" + property;
    String entity;
    if (value instanceof String)
      entity = "\"" + value + "\"";
    else 
      entity = value.toString();
    int result = Neo4JConnectionManager.getInstance()
            .getConnection()
            .putJSON(nodeEntryPointUri, entity);
    if (result == 204)
      return 0;
    else
      return -1;
  }

  public int addProperty(Relationship rel, String property, Object value)
  {
    String nodeEntryPointUri = rel.getSelf() + "/properties";
    String entity = toJsonNameValuePairCollection(property, value);
    int result = Neo4JConnectionManager.getInstance()
            .getConnection()
            .putJSON(nodeEntryPointUri, entity);
    if (result == 204)
      return 0;
    else
      return -1;
  }

  private JSONParser getParser()
  {
    if (jparser == null)
      jparser = new JSONParser();
    return jparser;
  }

  

  private JSONObject get()
  {
    Neo4JConnection connection = Neo4JConnectionManager.getInstance().getConnection();
    String response = connection.get("");
    JSONObject result = null;
    try
    {
      result = (JSONObject) getParser().parse(response);
    }
    catch (ParseException ex)
    {
      logger.log(Level.SEVERE, "Error parsing response " + response.substring(0, 20), ex);
    }
    return result;
  }
  
  public int removeNode()
  {
    return -1;
  } 
  
  public int removeRelationship()
  {
    return -1;
  } 
}
