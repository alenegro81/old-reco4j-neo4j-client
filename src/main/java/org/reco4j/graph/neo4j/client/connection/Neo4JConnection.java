/*
 * Neo4JConnection.java
 * 
 * Copyright (C) 2012 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j.client.connection;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import java.io.InputStream;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Alessandro Negro <alessandro.negro at reco4j.org>
 */
public class Neo4JConnection
{

  private static final Logger logger = Logger.getLogger(Neo4JConnection.class.getName());
  private Neo4JConfiguration conf;
  private final String serverRootURI;

  public Neo4JConnection(Neo4JConfiguration conf)
  {
    this.conf = conf;
    this.serverRootURI = (String) this.conf.get(Neo4JConfiguration.SERVER_ROOT_URI);

  }

  public boolean checkConnection()
  {
    ClientResponse response = null;
    try
    {
      response = sumbitSimpleGet(null);
      if (response != null)
        return true;
      else
        return false;
    }
    finally
    {
      if (response != null)
        response.close();
    }
  }

  public String get(String path)
  {
    ClientResponse response = null;
    try
    {
      response = sumbitJSONRequest(removeServerPath(path), "GET");
      if (response != null)
        return response.getEntity(String.class);
      else
        return null;
    }
    finally
    {
      if (response != null)
        response.close();
    }
  }

  public String post(String path)
  {
    ClientResponse response = null;
    try
    {
      response = sumbitJSONRequest(removeServerPath(path), "POST");
      if (response != null)
        return response.getEntity(String.class);
      else
        return null;
    }
    finally
    {
      if (response != null)
        response.close();
    }
  }

  public InputStream postInputStream(String path)
  {
    ClientResponse response = null;

    response = sumbitJSONRequest(removeServerPath(path), "POST");
    if (response != null)
      return response.getEntity(InputStream.class);
    else
      return null;
  }

  public URI postJSON(String path, String entity)
  {
    ClientResponse response = null;
    try
    {
      response = pushJSONRequest(removeServerPath(path), entity);
      if (response != null)
      {
        System.err.println(response);
        return response.getLocation();
      }
      else
        return null;
    }
    finally
    {
      if (response != null)
        response.close();
    }

  }

  public int putJSON(String path, String entity)
  {
    ClientResponse response = null;
    response = putJSONRequest(removeServerPath(path), entity);
    int res = response.getStatus();
    response.close();
    return res;
  }

  private ClientResponse sumbitSimpleGet(String path) throws ClientHandlerException, UniformInterfaceException
  {
    ClientResponse response = null;
    String url = serverRootURI;
    if (path != null)
      url += path;
    try
    {
      WebResource resource = Client.create().resource(url);
      response = resource.get(ClientResponse.class);
      if (response.getStatus() == 200)
        return response;
      else
      {
        logger.log(Level.SEVERE, "Server ({0}) return error: {1}", new Object[]
                {
                  serverRootURI, response.getStatus()
                });
        response.close();
        return null;
      }
    }
    catch (Exception ex)
    {
      logger.log(Level.SEVERE, "Error connecting the Neo4J server (" + serverRootURI + ")", ex);
      if (response != null)
        response.close();
      return null;
    }
  }

  private ClientResponse sumbitJSONRequest(String path, String type) throws ClientHandlerException, UniformInterfaceException
  {
    ClientResponse response = null;
    String url = serverRootURI;
    if (path != null)
      url += path;
    logger.log(Level.INFO, "Contacting {0} ... ", url);
    try
    {
      WebResource resource = Client.create().resource(url);
      if (type.equalsIgnoreCase("POST"))
        response = resource.accept(MediaType.APPLICATION_JSON)
                .post(ClientResponse.class);
      else
        response = resource.accept(MediaType.APPLICATION_JSON)
                .get(ClientResponse.class);
      if (response.getStatus() == 200)
        return response;
      else
      {
        logger.log(Level.SEVERE, "Server ({0}) return error: {1}", new Object[]
                {
                  url, response.getStatus()
                });
        response.close();
        return null;
      }
    }
    catch (Exception ex)
    {
      logger.log(Level.SEVERE, "Error connecting the Neo4J server (" + serverRootURI + ")", ex);
      if (response != null)
        response.close();
      return null;
    }
  }

  private ClientResponse pushJSONRequest(String path, String entity)
  {
    ClientResponse response = null;
    String url = serverRootURI;
    if (path != null)
      url += path;
    logger.log(Level.INFO, "Contacting {0} ... ", url);
    try
    {
      WebResource resource = Client.create().resource(url);
      response = resource.accept(MediaType.APPLICATION_JSON)
              .type(MediaType.APPLICATION_JSON)
              .entity(entity)
              .post(ClientResponse.class);
      if (response.getStatus() == 200 || response.getStatus() == 201)
        return response;
      else
      {
        logger.log(Level.SEVERE, "Server ({0}) return error: {1}", new Object[]
                {
                  url, response.getStatus()
                });
        response.close();
        return null;
      }
    }
    catch (Exception ex)
    {
      logger.log(Level.SEVERE, "Error connecting the Neo4J server (" + serverRootURI + ")", ex);
      if (response != null)
        response.close();
      return null;
    }
  }

  private ClientResponse putJSONRequest(String path, String entity)
  {
    ClientResponse response = null;
    String url = serverRootURI;
    if (path != null)
      url += path;
    logger.log(Level.INFO, "Contacting {0} ... ", url);
    try
    {
      WebResource resource = Client.create().resource(url);
      response = resource.accept(MediaType.APPLICATION_JSON)
              .type(MediaType.APPLICATION_JSON)
              .entity(entity)
              .put(ClientResponse.class);
      if (response.getStatus() == 200 || response.getStatus() == 201 || response.getStatus() == 204)
        return response;
      else
      {
        logger.log(Level.SEVERE, "Server ({0}) return error: {1}", new Object[]
                {
                  url, response.getStatus()
                });
        return response;
      }
    }
    catch (Exception ex)
    {
      logger.log(Level.SEVERE, "Error connecting the Neo4J server (" + serverRootURI + ")", ex);
      if (response != null)
        response.close();
      return null;
    }
  }

  private String removeServerPath(String path)
  {
    String newPath;
    if (path.startsWith("http"))
      newPath = path.replaceAll(serverRootURI, "");
    else
      newPath = path;
    return newPath;
  }
}
