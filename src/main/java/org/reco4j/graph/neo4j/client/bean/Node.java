/*
 * Node.java
 * 
 * Copyright (C) 2012 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j.client.bean;

import org.json.simple.JSONObject;

/**
 *
 * @author Alessandro Negro <alessandro.negro at reco4j.org>
 */
public class Node
{

  private String self;
  private JSONObject data;
  private long id;

  public String getSelf()
  {
    return self;
  }

  public void setSelf(String self)
  {
    this.self = self;
    if (self != null)
      id = Long.parseLong(self.substring(self.lastIndexOf("/") + 1));
  }

  public JSONObject getData()
  {
    return data;
  }

  public void setData(JSONObject data)
  {
    this.data = data;
  }

  public long getId()
  {
    return id;
  }

  public String getProperty(String name)
  {
    return (String) data.get(name);
  }
}
