/*
 * Neo4JConfiguration.java
 * 
 * Copyright (C) 2012 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j.client.connection;

import java.util.HashMap;

/**
 *
 * @author Alessandro Negro <alessandro.negro at reco4j.org>
 */
public class Neo4JConfiguration
{
  private HashMap<String, Object> properties;
  public static final String SERVER_ROOT_URI = "SERVER_ROOT_URI";

  public Neo4JConfiguration()
  {
    properties = new HashMap<String, Object>();
  }   
  
  public Object get(String property)
  {
    return properties.get(property);    
  }
  
  public void set(String property, Object value)
  {
    properties.put(property, value);
  }
  
}
