/*
 * Neo4JTestClient.java
 * 
 * Copyright (C) 2012 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j.client.examples;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.reco4j.graph.neo4j.client.Neo4JClient;
import org.reco4j.graph.neo4j.client.bean.Plugin;
import org.reco4j.graph.neo4j.client.connection.Neo4JConfiguration;

/**
 *
 * @author Alessandro Negro <alessandro.negro at reco4j.org>
 */
public class Neo4JTestClient
{

  private static final String SERVER_ROOT_URI = "http://localhost:7474/db/data/";
  public static final String RECO4J_RECOMMENDER_PLUGIN = "Reco4jRecommender";
  public static final String RECO4j_GET_ITEMS = "get_items";
  public static final String RECO4j_GET_USERS = "get_users";
  public static final String RECO4j_GET_ALL_RATINGS = "get_all_ratings";

  public static void main(String[] args) throws URISyntaxException, IOException
  {
    Neo4JConfiguration conf = new Neo4JConfiguration();
    conf.set(Neo4JConfiguration.SERVER_ROOT_URI, SERVER_ROOT_URI);


    Neo4JClient client = new Neo4JClient(conf);
    Plugin plugin = client.getPlugin(RECO4J_RECOMMENDER_PLUGIN);
    System.out.println(plugin);
    System.out.println(plugin.getContent(RECO4j_GET_ITEMS));
    System.out.println(plugin.getContent(RECO4j_GET_USERS));
    System.out.println(plugin.getContent(RECO4j_GET_ALL_RATINGS));
    //client.getNodes(plugin.getContent(RECO4j_GET_USERS));
    //client.getNodes(plugin.getContent(RECO4j_GET_ITEMS));
//    List<Relationship> relationships = client.getRelationships(plugin.getContent(RECO4j_GET_ALL_RATINGS));
//    for (Relationship rel : relationships)
//    {
//      System.out.println(rel.getStartId() + "\t" + rel.getEndId() + "\t" + rel.getProperty("RankValue"));
//    }
//    System.out.println(client.addNode());

    InputStream relationships = null;
    try
    {
      relationships = client.getInputStream(plugin.getContent(RECO4j_GET_ALL_RATINGS));
      JsonFactory jsonFactory = new JsonFactory(); 
      JsonParser jParser = jsonFactory.createJsonParser(relationships);
      while (jParser.nextToken() != JsonToken.END_ARRAY)
      {
        String start = "", end = "", rank = "", self = "";
        while (true)
        {
          if (jParser.nextToken() == JsonToken.END_OBJECT)
          {
            System.out.println(self + "\t" + start + "\t" + end + "\t" + rank);
            start = "";
            end = "";
            rank = "";
            self = "";
            break;
          }
          String fieldname = jParser.getCurrentName();
          if ("data".equalsIgnoreCase(fieldname))
          {
            while (jParser.nextToken() != JsonToken.END_OBJECT)
            {
              String dataFieldName = jParser.getCurrentName();
              if ("rankValue".equalsIgnoreCase(dataFieldName))
              {
                jParser.nextToken();
                rank = jParser.getText();
              }
            }
          }
          if ("extensions".equalsIgnoreCase(fieldname))
          {
            while (jParser.nextToken() != JsonToken.END_OBJECT)
            {
              //do nothing
            }
          }
          if ("start".equals(fieldname))
          {
            jParser.nextToken();
            start = getId(jParser.getText());
          }
          if ("self".equals(fieldname))
          {
            jParser.nextToken();
            self = getId(jParser.getText());
          }
          if ("end".equals(fieldname))
          {
            jParser.nextToken();
            end = getId(jParser.getText());
          }
        }
      }
      jParser.close();

    }
    catch (JsonGenerationException e)
    {
      e.printStackTrace();
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
    finally
    {
      if (relationships != null)
        relationships.close();
    }
  }
  public static String getId(String url)
  {
    return url.substring(url.lastIndexOf("/") + 1);
  }
}
