/*
 * Neo4JConnectionManager.java
 * 
 * Copyright (C) 2012 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j.client.connection;

/**
 *
 * @author Alessandro Negro <alessandro.negro at reco4j.org>
 */
public class Neo4JConnectionManager
{
  private Neo4JConfiguration dbConfiguration;
  
  private Neo4JConnectionManager()
  {
  }
  
  public static Neo4JConnectionManager getInstance()
  {
    return Neo4JConnectionManagerHolder.INSTANCE;
  }
  
  private static class Neo4JConnectionManagerHolder
  {

    private static final Neo4JConnectionManager INSTANCE = new Neo4JConnectionManager();
  }

  public Neo4JConfiguration getDbConfiguration()
  {
    return dbConfiguration;
  }

  public void setDbConfiguration(Neo4JConfiguration dbConfiguration)
  {
    this.dbConfiguration = dbConfiguration;
  }
  
  public Neo4JConnection getConnection()
  {
    //This must be implemented to support pooling/ha/fault tolerance
    return new Neo4JConnection(dbConfiguration);
  }
}
