/*
 * Relationship.java
 * 
 * Copyright (C) 2012 Alessandro Negro <alessandro.negro at reco4j.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.reco4j.graph.neo4j.client.bean;

import org.json.simple.JSONObject;

/**
 *
 * @author Alessandro Negro <alessandro.negro at reco4j.org>
 */
  public final class Relationship
{

  public static final String OUT = "out";
  public static final String IN = "in";
  public static final String BOTH = "both";
  private long id;
  private String self;
  private String type;
  private String start;
  private long startId;
  private String end;
  private long endId;
  private String direction;
  private JSONObject data;

  public String toJsonCollection()
  {
    StringBuilder sb = new StringBuilder();
    sb.append("{ ");
    sb.append(" \"type\" : \"").append(type).append("\"");
    if (direction != null)
    {
      sb.append(", \"direction\" : \"").append(direction).append("\"");
    }
    sb.append(" }");
    return sb.toString();
  }

  public Relationship()
  {
  }

  public Relationship(String type, String direction)
  {
    setType(type);
    setDirection(direction);
  }

  public Relationship(String type)
  {
    this(type, null);
  }

  public final void setType(String type)
  {
    this.type = type;
  }

  public void setDirection(String direction)
  {
    this.direction = direction;
  }

  public String getStart()
  {
    return start;
  }

  public void setStart(String start)
  {
    this.start = start;
    if (start != null)
      startId = Long.parseLong(start.substring(start.lastIndexOf("/") + 1));
  }

  public String getEnd()
  {
    return end;
  }

  public void setEnd(String end)
  {
    this.end = end;
    if (end != null)
      endId = Long.parseLong(end.substring(end.lastIndexOf("/") + 1));
  }

  public JSONObject getData()
  {
    return data;
  }

  public void setData(JSONObject data)
  {
    this.data = data;
  }

  public String getType()
  {
    return type;
  }

  public long getStartId()
  {
    return startId;
  }

  public long getEndId()
  {
    return endId;
  }

  public Object getProperty(String name)
  {
    return data.get(name);
  }

  public String getSelf()
  {
    return self;
  }

  public void setSelf(String self)
  {
    this.self = self;
    if (self != null)
      id = Long.parseLong(self.substring(self.lastIndexOf("/") + 1));
  }
}
