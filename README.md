Reco4J Neo4J client

This jar contains all the classes needed to access Neo4J by means of it REST api.
We use a combination of jersey + json simple + jackson to query a Neo4J server 
and decode the result in bean or into string without need to decode the result
by your own.

The entry class is: Neo4JClient.

In the Examples package there is a sample use of the client, in particular we show how to use jackson to 
decode the results provided by Neo4J taht coudl be very larg so some time they 
can be put into memory.

Moreover in the test package you'll find a complete list of example, one for each 
method in Neo4JClient.

We start develop it since we need a client for Reco4J but it can be use 
as a general purpose Neo4J java client. Since from what I know there is a real 
java client for it.